package com.ocr.anthony;

public class Order {
    /**
     * Display all available menus in the restaurant.
     */
    public void displayAvailableMenu() {
        System.out.println("Choix menu :");
        System.out.println("1 - Falafel");
        System.out.println("2 - Houmous");
        System.out.println("3 - Tofu");
        System.out.println("Que voulez-vous comme menu ? ");

    }
    /**
     * Display a selected menu.
     * @param nbMenu The selected menu.
     */
    public void displaySelectedMenu(int nbMenu) {
        System.out.println("Vous avez choisi le menu " + nbMenu);
    }
}